package com.github.bitlinker.imagewallpaper.business;

import android.net.Uri;

import com.github.bitlinker.imagewallpaper.data.ImagePickerRepo;
import com.github.bitlinker.imagewallpaper.data.SettingsRepo;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class ImageListInteractor {
    private final SettingsRepo settingsRepo;
    private final ImagePickerRepo imagePickerRepo;

    public ImageListInteractor(SettingsRepo settingsRepo,
                               ImagePickerRepo imagePickerRepo) {
        this.settingsRepo = settingsRepo;
        this.imagePickerRepo = imagePickerRepo;
    }

    public Single<List<ImageInfo>> getImageList() {
        // TODO: threading
        return Single.just(settingsRepo.getImageList());
    }

    public Completable deleteImages(List<ImageInfo> images) {
        return Completable.complete(); // TODO
    }

    public Completable addImagesByUris(List<String> imageUris) {
        return Single.just(imageUris)
                .map(uris -> {
                    List<ImageInfo> imagesList = new ArrayList<>();
                    for (String uri : uris) {
                        imagesList.add(imagePickerRepo.createImageInfoFromUri(uri));
                    }
                    return imagesList;
                })
                .ignoreElement(); // TODO: save it!
    }
}
