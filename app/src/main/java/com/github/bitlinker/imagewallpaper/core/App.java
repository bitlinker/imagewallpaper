package com.github.bitlinker.imagewallpaper.core;

import android.app.Application;

import com.github.bitlinker.imagewallpaper.BuildConfig;
import com.github.bitlinker.imagewallpaper.di.Injector;

import androidx.annotation.NonNull;

import timber.log.Timber;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        initLogging();
        initDI();
    }

    private void initLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initDI() {
        Injector.getInstance().createAppScope(this);
    }
}
