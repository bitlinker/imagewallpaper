package com.github.bitlinker.imagewallpaper.core;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class ImageWallpaperAppGlideModule extends AppGlideModule {
}
