package com.github.bitlinker.imagewallpaper.data;

import android.os.Handler;

import com.github.bitlinker.imagewallpaper.domain.ImageInfo;
import com.github.bitlinker.imagewallpaper.data.wallpaper.DirectImageChangingOrder;
import com.github.bitlinker.imagewallpaper.data.wallpaper.ImageChangingOrder;
import com.github.bitlinker.imagewallpaper.data.wallpaper.RandomImageChangingOrder;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import timber.log.Timber;

public class ImageChangingManager {
    private final SettingsRepo settings;

    private ImageChangingOrder imageChangingOrder;
    private boolean lastImageChangingMode;
    private int lastListHash;
    private long lastUpdateTime;

    private Handler handler;


    Subject<ImageInfo> imageSubject = BehaviorSubject.create();

    private final Runnable updateImageRunnable = this::updateImage;

    public ImageChangingManager(SettingsRepo settings) {
        // TODO: settings provider?
        this.settings = settings;
        handler = new Handler();
        checkSettingsChanged();
        updateImage();
    }

    public void checkSettingsChanged() {
        List<ImageInfo> imageList = settings.getImageList();

        // TODO: color, tilt

        boolean curImageChangingMode = settings.getRandomOrder();
        if (curImageChangingMode != lastImageChangingMode) { // Image changing mode is changed
            imageChangingOrder = null;
        }
        int curListHash = imageList.hashCode();
        if (lastListHash == 0 || curListHash != lastListHash) { // List is changed
            imageChangingOrder = null;
        }

        if (imageChangingOrder == null) {
            imageChangingOrder = curImageChangingMode ?
                    new RandomImageChangingOrder(imageList) :
                    new DirectImageChangingOrder(imageList);
            lastImageChangingMode = curImageChangingMode;
            lastListHash = curListHash;
        }

        // TODO: reschedule update
    }

    public void onResume() {

    }

    public void onPause() {

    }

    private void rescheduleImageUpdate(long interval) {
        handler.removeCallbacks(updateImageRunnable);
        handler.postDelayed(updateImageRunnable, interval);
    }

    public void updateImage() {
        Timber.d("updateImage()");
        imageSubject.onNext(imageChangingOrder.next());
        rescheduleImageUpdate(settings.getSwitchInterval());
        lastUpdateTime = System.currentTimeMillis();
    }

    public @NonNull Observable<ImageInfo> getImageObservable() {
        return imageSubject;
    }
}
