package com.github.bitlinker.imagewallpaper.data;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import java.io.IOException;

public class ImagePickerRepo {
    private final Context context;

    public ImagePickerRepo(Context context) {
        this.context = context;
    }

    public ImageInfo createImageInfoFromUri(String uriStr) throws IOException {
        Uri uri = Uri.parse(uriStr);
        String[] proj = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.DISPLAY_NAME};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    int dataColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    int nameColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
                    String path = cursor.getString(dataColumnIndex);
                    String name = cursor.getString(nameColumnIndex);
                    return new ImageInfo(path, name);
                }
            } finally {
                cursor.close();
            }
        }
        throw new IOException("Failed to get image info from URI: " + uriStr);
    }
}
