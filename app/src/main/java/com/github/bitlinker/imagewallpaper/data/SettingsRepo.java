package com.github.bitlinker.imagewallpaper.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.github.bitlinker.imagewallpaper.R;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;
import com.github.bitlinker.imagewallpaper.domain.ImageScaleMode;

import net.margaritov.preference.colorpicker.ColorPickerPreference;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class SettingsRepo {
    private final Context context;
    private final SharedPreferences preferences;

    private Map<String, ImageScaleMode> imageScaleModeMap = new HashMap<>();

    public SettingsRepo(@NonNull Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        imageScaleModeMap.put(context.getString(R.string.pref_image_scale_mode_fit), ImageScaleMode.FIT);
        imageScaleModeMap.put(context.getString(R.string.pref_image_scale_mode_fill), ImageScaleMode.FILL);
    }

    public @NonNull ImageScaleMode getImageScaleMode() {
        String string = preferences.getString(context.getString(R.string.pref_image_scale_mode), context.getString(R.string.pref_image_scale_mode_default));
        ImageScaleMode imageScaleMode = imageScaleModeMap.get(string);
        if (imageScaleMode == null) {
            throw new IllegalStateException("Unknown image scale mode: " + string);
        }
        return imageScaleMode;
    }

    public long getSwitchInterval() {
        //DBG
        //return 10000L;
        return parseInt(preferences.getString(context.getString(R.string.pref_image_switch_interval), context.getString(R.string.pref_image_switch_interval_default))) * 60L * 1000L;
    }

    public boolean getRandomOrder() {
        return preferences.getBoolean(context.getString(R.string.pref_random_order), getBooleanDefault(R.string.pref_random_order_default));
    }

    public boolean getTiltAnimation() {
        return preferences.getBoolean(context.getString(R.string.pref_tilt_animation), getBooleanDefault(R.string.pref_tilt_animation_default));
    }

    public boolean getDblClickToChange() {
        return preferences.getBoolean(context.getString(R.string.pref_dblclick_change), getBooleanDefault(R.string.pref_dblclick_change_default));
    }

    private boolean getBooleanDefault(int defStringId) {
        String string = context.getString(defStringId);
        return Boolean.parseBoolean(string);
    }

    private int parseInt(@NonNull String string) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Unknown value: ", e);
        }
    }

    public @NonNull
    List<ImageInfo> getImageList() {
        // TODO: store
//        List<ImageInfo> images = new ArrayList<>();
//        images.add(new ImageInfo("http://netghost.narod.ru/gff/sample/images/png/marble24.png"));
//        images.add(new ImageInfo("https://media.wired.com/photos/5b45021f3808c83da3503cc7/4:3/w_660,c_limit/tumblr_inline_mjx5ioXh8l1qz4rgp.gif"));
//        images.add(new ImageInfo("https://safebooru.org/images/2549/cd010cd0e77a17ce1401eddb865bf8620ca895d1.gif"));
//        return images;

        String string = preferences.getString(context.getString(R.string.pref_images_list), "");
        if (!TextUtils.isEmpty(string)) {
            byte[] data = Base64.decode(string, Base64.DEFAULT);
            try {
                try (ObjectInputStream is = new ObjectInputStream(new ByteArrayInputStream(data))) {
                    //noinspection unchecked
                    return (List<ImageInfo>) is.readObject();
                }
            } catch (IOException | ClassNotFoundException | ClassCastException e) {
                Timber.w(e);
            }
        }
        return Collections.emptyList();
    }

    public void setImageList(@NonNull List<ImageInfo> images) {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(stream)) {
                oos.writeObject(images);
            } catch (IOException e) {
                Timber.w(e);
            }
            byte[] bytes = stream.toByteArray();
            String data = Base64.encodeToString(bytes, Base64.DEFAULT);
            preferences
                    .edit()
                    .putString(context.getString(R.string.pref_images_list), data)
                    .apply();
        } catch (IOException e) {
            Timber.w(e);
        }
    }

    public int getBackgroundColor() {
        return preferences.getInt(
                context.getString(R.string.pref_rendering_background_color),
                ColorPickerPreference.convertToColorInt(context.getString(R.string.pref_rendering_background_color_default))
        );
    }
}
