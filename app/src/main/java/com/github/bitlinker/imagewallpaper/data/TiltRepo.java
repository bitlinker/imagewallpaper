package com.github.bitlinker.imagewallpaper.data;

import android.content.Context;
import android.graphics.PointF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import androidx.annotation.NonNull;
import io.reactivex.Observable;
import timber.log.Timber;

public class TiltRepo {
    private final SensorManager sensorManager;
    private Sensor accelerometer;
    private Observable<PointF> tiltObservable;

    public TiltRepo(@NonNull Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        }

        if (accelerometer != null && sensorManager != null) {
            tiltObservable = Observable.create(emitter -> {
                final SensorEventListener listener = new SensorEventListener() {
                    @Override
                    public void onSensorChanged(SensorEvent event) {
                        float tiltX = event.values[0] / SensorManager.GRAVITY_EARTH;
                        float tiltY = event.values[1] / SensorManager.GRAVITY_EARTH;
                        Timber.d("Tilt: %f, %f", tiltX, tiltY);
                        emitter.onNext(new PointF(tiltX, tiltY));
                    }

                    @Override
                    public void onAccuracyChanged(Sensor sensor, int accuracy) {
                        // Do nothing...
                    }
                };
                emitter.setCancellable(() -> {
                    Timber.d("unregisterListener()");
                    sensorManager.unregisterListener(listener);
                });
                Timber.d("registerListener()");
                // SENSOR_DELAY_UI is low for UI
                sensorManager.registerListener(listener, accelerometer, SensorManager.SENSOR_DELAY_GAME);
            });
        } else {
            tiltObservable = Observable.just(new PointF(0.F, 0.F));
        }
        tiltObservable = tiltObservable.share();
    }

    /**
     * Returns the tilt vector in range [-1 ... 1]. X is
     *
     * @return tilt listener
     */
    public @NonNull Observable<PointF> getTilt() {
        return tiltObservable;
    }
}
