package com.github.bitlinker.imagewallpaper.data.schedulers;

import io.reactivex.Scheduler;

public interface SchedulerProvider {
    Scheduler ui();
    Scheduler io();
}
