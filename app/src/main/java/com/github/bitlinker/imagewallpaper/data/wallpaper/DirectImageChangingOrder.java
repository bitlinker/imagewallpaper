package com.github.bitlinker.imagewallpaper.data.wallpaper;

import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DirectImageChangingOrder implements ImageChangingOrder {
    private int currentIndex = 0;
    private final List<ImageInfo> images;

    public DirectImageChangingOrder(List<ImageInfo> images) {
        this.images = new ArrayList<>(images);
    }

    @Override
    public ImageInfo next() {
        if (images.isEmpty()) return ImageInfo.EMPTY;

        ImageInfo imageInfo = images.get(currentIndex++);
        if (currentIndex >= images.size()) {
            Collections.shuffle(images);
            currentIndex = 0;
        }
        return imageInfo;
    }
}
