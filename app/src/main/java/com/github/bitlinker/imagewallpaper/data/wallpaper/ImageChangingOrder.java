package com.github.bitlinker.imagewallpaper.data.wallpaper;

import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

public interface ImageChangingOrder {
    ImageInfo next();
}
