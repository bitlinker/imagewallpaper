package com.github.bitlinker.imagewallpaper.data.wallpaper;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.github.bitlinker.imagewallpaper.R;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.reactivex.Observable;

public class ImageLoader {
    private final Context context;

    public ImageLoader(@NonNull Context context) {
        this.context = context;
    }

    public @NonNull Observable<Drawable> load(@NonNull ImageInfo imageInfo, int size) {
//        return Observable.create(emitter -> {
//            GlideApp
//                    .with(context)
//                    .load(imageInfo.getUrl())
//                    .error(R.drawable.intent)
//                    .into(new Target<Drawable>() {
//
//                        @Override
//                        public void onLoadStarted(@Nullable Drawable placeholder) {
//                            if (placeholder != null && !emitter.isDisposed()) {
//                                emitter.onNext(placeholder);
//                            }
//                        }
//
//                        @Override
//                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
//                            if (!emitter.isDisposed()) {
//                                if (errorDrawable != null) {
//                                    emitter.onNext(errorDrawable);
//                                }
//                                emitter.onComplete();
//                            }
//                        }
//
//                        @Override
//                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                            if (!emitter.isDisposed()) {
//                                emitter.onNext(resource);
//                                emitter.onComplete();
//                            }
//                        }
//
//                        @Override
//                        public void onLoadCleared(@Nullable Drawable placeholder) {
//                            if (!emitter.isDisposed()) {
//                                if (placeholder != null) {
//                                    emitter.onNext(placeholder);
//                                }
//                                emitter.onComplete();
//                            }
//                        }
//
//                        @Override
//                        public void getSize(@NonNull SizeReadyCallback cb) {
//                            // TODO: options
//                            cb.onSizeReady(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
//                        }
//
//                        @Override
//                        public void removeCallback(@NonNull SizeReadyCallback cb) {
//                            // Do nothing...
//                        }
//
//                        @Override
//                        public void setRequest(@Nullable Request request) {
//                            // Do nothing...
//                        }
//
//                        @Override
//                        public @Nullable Request getRequest() {
//                            // Do nothing...
//                            return null;
//                        }
//
//                        @Override
//                        public void onStart() {
//                            // Do nothing...
//                        }
//
//                        @Override
//                        public void onStop() {
//                            // Do nothing...
//                        }
//
//                        @Override
//                        public void onDestroy() {
//                            // Do nothing...
//                        }
//                    });
//            // TODO: on load error; on in progress
//            //.apply(RequestOptions.errorOf(R.mipmap.ic_launcher))
//                    /*.addListener(new RequestListener<Drawable>() {
//                        @Override
//                        public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                            Timber.d("Image failed");
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(final Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                            Timber.d("Image ready: " + resource.getIntrinsicWidth() + " x " + resource.getIntrinsicHeight());
//                            handler.post(() -> onImageLoaded(resource));
//                            return false;
//                        }
//
//                    })
//                    .into()
//                    //.submit(); // TODO: option?
//                    .submit(size, size);*/
//        });
        return Observable.never();
    }
}
