package com.github.bitlinker.imagewallpaper.data.wallpaper;

import android.graphics.Rect;

import com.github.bitlinker.imagewallpaper.domain.ImageScaleMode;

import androidx.annotation.NonNull;

public class ImageScaleCalculator {
    private Rect calcFitRect(int width, int height, int imgWidth, int imgHeight) {
        float imgAspect = (float) imgWidth / (float) imgHeight;
        float wndAspect = (float) width / (float) height;
        if (imgAspect > wndAspect) {
            float imgScale = (float) width / (float) imgWidth;
            int h = (int) (imgScale * imgHeight);
            int offsetY = (height - h) / 2;
            return new Rect(0, offsetY, width, offsetY + h);
        } else {
            float imgScale = (float) height / (float) imgHeight;
            int w = (int) (imgScale * imgWidth);
            int offsetX = (width - w) / 2;
            return new Rect(offsetX, 0, offsetX + w, height);
        }
    }

    // TODO: common methods?
    private Rect calcFillRect(int width, int height, int imgWidth, int imgHeight) {
        float imgAspect = (float) imgWidth / (float) imgHeight;
        float wndAspect = (float) width / (float) height;
        if (imgAspect > wndAspect) {
            float imgScale = (float) height / (float) imgHeight;
            int w = (int) (imgScale * imgWidth);
            int offsetX = (width - w) / 2;
            return new Rect(offsetX, 0, offsetX + w, height);
        } else {
            float imgScale = (float) width / (float) imgWidth;
            int h = (int) (imgScale * imgHeight);
            int offsetY = (height - h) / 2;
            return new Rect(0, offsetY, width, offsetY + h);
        }
    }

    public @NonNull Rect calc(@NonNull ImageScaleMode imageScaleMode,
                              int width,
                              int height,
                              int imageWidth,
                              int imageHeight,
                              float tiltX,
                              float tiltY
    ) {
        Rect rc;
        if (ImageScaleMode.FILL.equals(imageScaleMode)) {
            rc = calcFillRect(width, height, imageWidth, imageHeight);
        } else {
            rc = calcFitRect(width, height, imageWidth, imageHeight);
        }
        rc.offset((int) (tiltX * 500), (int) (tiltY * 500));
        return rc;
    }
}
