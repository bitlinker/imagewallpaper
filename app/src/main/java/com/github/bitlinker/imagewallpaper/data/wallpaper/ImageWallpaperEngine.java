package com.github.bitlinker.imagewallpaper.data.wallpaper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.github.bitlinker.imagewallpaper.data.ImageChangingManager;
import com.github.bitlinker.imagewallpaper.data.SettingsRepo;
import com.github.bitlinker.imagewallpaper.data.TiltRepo;
import com.github.bitlinker.imagewallpaper.data.wallpaper.base.CustomWallpaperEngine;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;
import com.github.bitlinker.imagewallpaper.domain.ImageScaleMode;

import androidx.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;


// Engine is living the whole working time!
// Keep last image change date in instance
// Keep image changing order in instance, update only if list hashcode changed of if not exist
// Schedule update task on start, remove on pause


// TODO: autocloseable?
public class ImageWallpaperEngine extends CustomWallpaperEngine implements Drawable.Callback {
    // TODO: composition instead of inheritance
    private final ImageLoader imageLoader;
    private final TiltRepo tiltRepo;
    private final ImageScaleCalculator imageScaleCalculator;
    private final ImageChangingManager imageChangingManager;
    private final SettingsRepo settings;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final CompositeDisposable whileVisibleDisposable = new CompositeDisposable();

    private @Nullable
    Drawable image;
    private final TextDrawable noImageTextDrawable = new TextDrawable();
    private int bgColor;
    private @Nullable
    ImageScaleMode imageScaleMode;
    private @Nullable
    PointF tilt;

    public ImageWallpaperEngine(
            ImageLoader imageLoader,
            TiltRepo tiltRepo,
            ImageScaleCalculator imageScaleCalculator,
            SettingsRepo settings
    ) {
        Timber.d("ImageWallpaperEngine()");
        this.imageLoader = imageLoader;
        this.tiltRepo = tiltRepo;
        this.imageChangingManager = new ImageChangingManager(settings);
        this.imageScaleCalculator = imageScaleCalculator;
        this.settings = settings;

        noImageTextDrawable.setText("No image");
        noImageTextDrawable.setColor(Color.YELLOW);
        noImageTextDrawable.setTextSize(22F);

        Disposable subscribe = imageChangingManager // TODO: onResume?
                .getImageObservable()
                .subscribe(this::setImage, Timber::e);
        compositeDisposable.add(subscribe);
        updateSettings(settings);
    }

    private void setImage(ImageInfo imageInfo) {
        // TODO: load only if visible
        compositeDisposable.add(imageLoader.load(imageInfo, 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(drawable -> {
                    // TODO: clear prev drawable, if any

                    drawable.setCallback(this);
                    //drawable.setFilterBitmap(false); // TODO: toggle in settings if usable?
                    image = drawable;
                    setImageVisibility(isVisible());
                    scheduleRedraw();
                }, Timber::e));
    }

    private void checkSettingsUpdated() {

    }

    @Override
    public void onResume() {
        Timber.d("onResume()");
        checkSettingsUpdated();
        tilt = new PointF(0.F, 0.F);
        if (settings.getTiltAnimation()) {
            whileVisibleDisposable.add(
                    tiltRepo.getTilt().subscribe(tilt -> {
                        if (this.tilt.x != tilt.x || this.tilt.y != tilt.y) {
                            this.tilt = tilt;
                            scheduleRedraw();
                        }
                    }, Timber::e)
            );
        }
        setImageVisibility(true);
        super.onResume();
    }

    @Override
    public void onPause() {
        Timber.d("onPause()");
        whileVisibleDisposable.clear();
        setImageVisibility(false);
        super.onPause();
    }

    // TODO: rename start/resume
    private void setImageVisibility(boolean isVisible) {
        // TODO: what if image is owned from multiple engines? Wrap?
        if (image != null) {
            if (image instanceof GifDrawable) {
                final GifDrawable gifDrawable = (GifDrawable) image;
                if (isVisible) {
                    gifDrawable.setLoopCount(GifDrawable.LOOP_FOREVER);
                    gifDrawable.start();
                } else {
                    gifDrawable.stop();
                }
            }
            image.setVisible(isVisible, false);
        }
    }

    public void onDraw(Canvas canvas) {
        if (ImageScaleMode.FIT.equals(imageScaleMode)) {
            canvas.drawRGB(Color.red(bgColor), Color.green(bgColor), Color.blue(bgColor));
        }

        if (image != null) {
            Rect rect = imageScaleCalculator.calc(imageScaleMode, getWidth(), getHeight(), image.getIntrinsicWidth(), image.getIntrinsicHeight(), tilt.x, tilt.y);

            // TODO: initial size
            // TODO: relayout on demand, tilt multipliers
            image.setBounds(rect.left, rect.top, rect.right, rect.bottom);
            image.draw(canvas);
        } else {
            noImageTextDrawable.setBounds(getWidth() / 2, getHeight() / 2, getWidth(), getHeight());
            noImageTextDrawable.draw(canvas);
        }
    }

    @Override
    public void onDoubleTap() {
        super.onDoubleTap();
        // TODO: ensure list is not empty
        if (settings.getDblClickToChange()) { // TODO: cache; render settings object
            imageChangingManager.updateImage();
        }
    }

    @Override
    public void onResize(int width, int height) {
        Timber.d("onResize()");
        super.onResize(width, height);
        scheduleRedraw();
    }

    @Override
    public void invalidateDrawable(Drawable who) {
        //Timber.d("invalidateDrawable");
        scheduleRedraw();
    }

    @Override
    public void scheduleDrawable(Drawable who, Runnable what, long when) {
        //Timber.d("scheduleDrawable");
    }

    @Override
    public void unscheduleDrawable(Drawable who, Runnable what) {
        //Timber.d("unscheduleDrawable");
    }


    private void updateSettings(SettingsRepo settings) {
        bgColor = settings.getBackgroundColor();
        imageScaleMode = settings.getImageScaleMode();
    }

    @Override
    public void destroy() {
        Timber.d("destroy()");
        compositeDisposable.clear();
    }
}
