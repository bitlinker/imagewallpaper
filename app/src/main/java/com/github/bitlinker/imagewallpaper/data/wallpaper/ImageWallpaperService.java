package com.github.bitlinker.imagewallpaper.data.wallpaper;

import androidx.annotation.NonNull;

import com.github.bitlinker.imagewallpaper.data.SettingsRepo;
import com.github.bitlinker.imagewallpaper.data.TiltRepo;
import com.github.bitlinker.imagewallpaper.data.wallpaper.base.CustomWallpaperEngine;
import com.github.bitlinker.imagewallpaper.data.wallpaper.base.CustomWallpaperService;
import com.github.bitlinker.imagewallpaper.di.AppScope;
import com.github.bitlinker.imagewallpaper.di.Injector;

public class ImageWallpaperService extends CustomWallpaperService {
    @Override
    protected @NonNull
    CustomWallpaperEngine createEngine() {
        AppScope appScope = Injector.getInstance().getAppScope();

        SettingsRepo settings = appScope.provideSettingsRepo();
        TiltRepo tiltRepo = appScope.provideTiltRepo();
        ImageLoader imageLoader = appScope.provideImageLoader();

        return new ImageWallpaperEngine(
                imageLoader,
                tiltRepo,
                appScope.provideImageScaleCalculator(),
                settings
        );
    }
}
