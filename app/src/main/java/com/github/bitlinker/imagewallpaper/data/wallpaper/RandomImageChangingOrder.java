package com.github.bitlinker.imagewallpaper.data.wallpaper;

import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomImageChangingOrder implements ImageChangingOrder {
    private int currentIndex = 0;
    private final List<ImageInfo> shuffledImages;

    public RandomImageChangingOrder(List<ImageInfo> images) {
        shuffledImages = new ArrayList<>(images);
        Collections.shuffle(shuffledImages);
    }

    @Override
    public ImageInfo next() {
        if (shuffledImages.isEmpty()) return ImageInfo.EMPTY;

        ImageInfo imageInfo = shuffledImages.get(currentIndex++);
        if (currentIndex >= shuffledImages.size()) {
            Collections.shuffle(shuffledImages);
            currentIndex = 0;
        }
        return imageInfo;
    }
}
