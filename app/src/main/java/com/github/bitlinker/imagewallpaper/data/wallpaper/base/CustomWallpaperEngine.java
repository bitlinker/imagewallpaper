package com.github.bitlinker.imagewallpaper.data.wallpaper.base;

import android.graphics.Canvas;

import androidx.annotation.NonNull;

public abstract class CustomWallpaperEngine {
    public interface Callback {
        void scheduleRedraw();
    }

    private Callback callback;
    private int width;
    private int height;
    private boolean isVisible;

    /*package-private*/ void setCallback(@NonNull CustomWallpaperEngine.Callback callback) {
        this.callback = callback;
    }

    public void scheduleRedraw() {
        if (callback != null) {
            callback.scheduleRedraw();
        }
    }

    public void onResize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void onResume() {
        isVisible = true;
    }

    public void onPause() {
        isVisible = false;
    }

    protected abstract void onDraw(@NonNull Canvas canvas);

    public void onDoubleTap() {}

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public abstract void destroy();
}
