package com.github.bitlinker.imagewallpaper.data.wallpaper.base;

import android.graphics.Canvas;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import timber.log.Timber;

public abstract class CustomWallpaperService extends WallpaperService {

    protected abstract @NonNull CustomWallpaperEngine createEngine();

    @Override
    public Engine onCreateEngine() {
        Timber.d("onCreateEngine()");


        // Not working. TODO: find a way to check if lockscreen
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            WallpaperManager wpm = (WallpaperManager) getSystemService(WALLPAPER_SERVICE);
//            Timber.d("WP for lock: %s", wpm.getWallpaperId(WallpaperManager.FLAG_LOCK));
//            Timber.d("WP for desktop: %s", wpm.getWallpaperId(WallpaperManager.FLAG_SYSTEM));
//            Timber.d("file for lock: %s", wpm.getWallpaperFile(WallpaperManager.FLAG_LOCK));
//            Timber.d("file for desktop: %s", wpm.getWallpaperFile(WallpaperManager.FLAG_SYSTEM));
//        }

        return new Engine() {
            private final Handler handler = new Handler();
            private final CustomWallpaperEngine.Callback callback = new CustomWallpaperEngine.Callback() {
                @Override
                public void scheduleRedraw() {
                    handler.post(drawRunner);
                }
            };

            private @Nullable
            SurfaceHolder surfaceHolder;
            private @Nullable
            CustomWallpaperEngine customWallpaperEngine;

            private final Runnable drawRunner = this::draw;

            private final GestureDetector gestureDetector = new GestureDetector(CustomWallpaperService.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    Timber.d("Double tap!");
                    if (customWallpaperEngine != null) {
                        customWallpaperEngine.onDoubleTap();
                    }
                    return super.onDoubleTap(e);
                }
            });

            @Override
            public void onCreate(SurfaceHolder surfaceHolder) {
                super.onCreate(surfaceHolder);
                Timber.d("onCreate()");
                super.onCreate(surfaceHolder);
                this.surfaceHolder = surfaceHolder;
                customWallpaperEngine = createEngine();
                customWallpaperEngine.setCallback(callback);
            }

            @Override
            public void onDestroy() {
                Timber.d("onDestroy()");
                surfaceHolder = null;
                if (customWallpaperEngine != null) {
                    customWallpaperEngine.destroy();
                }
                super.onDestroy();
            }

            @Override
            public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                super.onSurfaceChanged(holder, format, width, height);
                Timber.d("onSurfaceChanged()");
                surfaceHolder = holder;
                if (customWallpaperEngine != null) {
                    customWallpaperEngine.onResize(width, height);
                }
                draw();
            }

            @Override
            public void onSurfaceRedrawNeeded(SurfaceHolder holder) {
                super.onSurfaceRedrawNeeded(holder);
                Timber.d("onSurfaceRedrawNeeded()");
                draw();
            }

            @Override
            public void onSurfaceCreated(SurfaceHolder holder) {
                super.onSurfaceCreated(holder);
                Timber.d("onSurfaceCreated()");
                surfaceHolder = holder;
            }

            @Override
            public void onSurfaceDestroyed(SurfaceHolder holder) {
                super.onSurfaceDestroyed(holder);
                surfaceHolder = null;
                handler.removeCallbacks(drawRunner);
            }

            @Override
            public void onVisibilityChanged(boolean visible) {
                super.onVisibilityChanged(visible);
                Timber.d("onVisibilityChanged: %s", visible);
                if (customWallpaperEngine != null) {
                    if (visible) {
                        customWallpaperEngine.onResume();
                    } else {
                        customWallpaperEngine.onPause();
                    }
                }
            }

            @Override
            public void onTouchEvent(MotionEvent event) {
                super.onTouchEvent(event);
                gestureDetector.onTouchEvent(event);
            }

            private void draw() {
                if (customWallpaperEngine != null && surfaceHolder != null && customWallpaperEngine.isVisible()) {
                    Canvas canvas = surfaceHolder.lockCanvas();
                    customWallpaperEngine.onDraw(canvas);
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (IllegalArgumentException e) {
                        Timber.e(e);
                    }
                }
                handler.removeCallbacks(drawRunner);
            }
        };
    }
}
