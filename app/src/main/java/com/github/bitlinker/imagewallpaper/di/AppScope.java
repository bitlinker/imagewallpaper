package com.github.bitlinker.imagewallpaper.di;

import android.content.Context;

import com.github.bitlinker.imagewallpaper.business.ImageListInteractor;
import com.github.bitlinker.imagewallpaper.data.ImagePickerRepo;
import com.github.bitlinker.imagewallpaper.data.SettingsRepo;
import com.github.bitlinker.imagewallpaper.data.TiltRepo;
import com.github.bitlinker.imagewallpaper.data.schedulers.AndroidSchedulerProvider;
import com.github.bitlinker.imagewallpaper.data.schedulers.SchedulerProvider;
import com.github.bitlinker.imagewallpaper.data.wallpaper.ImageLoader;
import com.github.bitlinker.imagewallpaper.data.wallpaper.ImageScaleCalculator;

import javax.annotation.Nullable;

public class AppScope {
    private final Context context;

    private @Nullable
    SettingsRepo settingsRepo;

    private @Nullable
    TiltRepo tiltRepo;

    private @Nullable
    ImageLoader imageLoader;

    private @Nullable
    ImageScaleCalculator imageScaleCalculator;

    private @Nullable
    ImagePickerRepo imagePickerRepo;

    private @Nullable
    ImageListInteractor imageListInteractor;

    private @Nullable
    SchedulerProvider schedulerProvider;

    public AppScope(Context context) {
        this.context = context;
    }

    public SettingsRepo provideSettingsRepo() {
        synchronized (this) {
            if (settingsRepo == null) {
                settingsRepo = new SettingsRepo(context);
            }
            return settingsRepo;
        }
    }

    public TiltRepo provideTiltRepo() {
        synchronized (this) {
            if (tiltRepo == null) {
                tiltRepo = new TiltRepo(context);
            }
            return tiltRepo;
        }
    }

    public Context provideContext() {
        return context;
    }

    public ImageLoader provideImageLoader() {
        synchronized (this) {
            if (imageLoader == null) {
                imageLoader = new ImageLoader(provideContext());
            }
            return imageLoader;
        }
    }

    public ImageScaleCalculator provideImageScaleCalculator() {
        synchronized (this) {
            if (imageScaleCalculator == null) {
                imageScaleCalculator = new ImageScaleCalculator();
            }
            return imageScaleCalculator;
        }
    }

    public ImagePickerRepo provideImagePickerRepo() {
        synchronized (this) {
            if (imagePickerRepo == null) {
                imagePickerRepo = new ImagePickerRepo(provideContext());
            }
            return imagePickerRepo;
        }
    }

    public ImageListInteractor provideImageListInteractor() {
        synchronized (this) {
            if (imageListInteractor == null) {
                imageListInteractor = new ImageListInteractor(provideSettingsRepo(), provideImagePickerRepo());
            }
        }
        return imageListInteractor;
    }

    public SchedulerProvider provideSchedulerProvider() {
        synchronized (this) {
            if (schedulerProvider == null) {
                schedulerProvider = new AndroidSchedulerProvider();
            }
            return schedulerProvider;
        }
    }
}
