package com.github.bitlinker.imagewallpaper.di;

import android.app.Activity;
import android.content.Context;

public class Injector {
    private static class InstanceHolder {
        private static Injector instance = new Injector();
    }

    public static Injector getInstance() {
        return InstanceHolder.instance;
    }

    private AppScope appScope;

    private Injector() {
    }

    public void createAppScope(Context context) {
        appScope = new AppScope(context);
    }

    public AppScope getAppScope() {
        return appScope;
    }

    public UiScope createUiScope(Activity activity) {
        return new UiScope(appScope, activity);
    }
}
