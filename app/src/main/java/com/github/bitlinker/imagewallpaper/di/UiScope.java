package com.github.bitlinker.imagewallpaper.di;

import android.app.Activity;

import com.github.bitlinker.imagewallpaper.ui.imagelist.ImageListPresenter;

import java.io.Closeable;
import java.io.IOException;

import javax.annotation.Nullable;

public class UiScope implements Closeable {
    private final AppScope appScope;
    private final Activity activity;
    private boolean isClosed = false;

    private @Nullable
    ImageListPresenter imageListPresenter;

    public UiScope(AppScope appScope, Activity activity) {
        this.appScope = appScope;
        this.activity = activity;
    }

    public ImageListPresenter provideImageListPresenter() {
        synchronized (this) {
            checkClosed();
            if (imageListPresenter == null) {
                imageListPresenter = new ImageListPresenter(appScope.provideImageListInteractor(), appScope.provideSchedulerProvider());
            }
            return imageListPresenter;
        }
    }

    private void checkClosed() {
        if (isClosed) {
            throw new IllegalStateException("The scope is closed");
        }
    }

    @Override
    public void close() throws IOException {
        synchronized (this) {
            isClosed = true;
        }
        // TODO?
    }
}
