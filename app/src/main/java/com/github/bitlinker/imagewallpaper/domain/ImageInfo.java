package com.github.bitlinker.imagewallpaper.domain;

import java.io.Serializable;

import androidx.annotation.NonNull;

import lombok.Data;

@Data
public class ImageInfo implements Serializable {
    public static final ImageInfo EMPTY = new ImageInfo("", "");

    private final String url;
    private final String name;
}
