package com.github.bitlinker.imagewallpaper.domain;

import lombok.Value;

@Value
public class ImageRenderSettings {
    ImageScaleMode imageScaleMode;
    boolean tiltAnimation;
    int backgroundColor;
}
