package com.github.bitlinker.imagewallpaper.domain;

public enum ImageScaleMode {
    FILL,
    FIT,
}
