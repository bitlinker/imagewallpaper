package com.github.bitlinker.imagewallpaper.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.github.bitlinker.imagewallpaper.R;

import pub.devrel.easypermissions.EasyPermissions;

public abstract class MandatoryPermissionActivity extends AppCompatActivity {
    public void onCreateWithPermissions() {
    }

//    public void onCreateWithNoPermissions() {
//        new AlertDialog.Builder(this)
//                .setMessage(R.string.settings_permissions_message)
//                .setTitle(R.string.settings_permissions_title)
//                .setOnDismissListener(dialog1 -> finish())
//                .show();
//    }

    // TODO: make it clean
    // TODO: request permissions

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onCreateWithPermissions();
//        compositeDisposable.add(rxPermissions
//                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
//                .subscribe(granted -> {
//                            if (granted) {
//                                onCreateWithPermissions();
//                            } else {
//                                onCreateWithNoPermissions();
//                            }
//                        },
//                        throwable -> {
//                            Timber.e(throwable);
//                            onCreateWithNoPermissions();
//                        }));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
