package com.github.bitlinker.imagewallpaper.ui.imagelist;

import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

public class ImageDiffer extends DiffUtil.ItemCallback<ImageInfo> {

    @Override
    public boolean areItemsTheSame(@NonNull ImageInfo oldItem, @NonNull ImageInfo newItem) {
        return oldItem.getUrl().equals(newItem.getUrl());
    }

    @Override
    public boolean areContentsTheSame(@NonNull ImageInfo oldItem, @NonNull ImageInfo newItem) {
        return oldItem.equals(newItem);
    }


}
