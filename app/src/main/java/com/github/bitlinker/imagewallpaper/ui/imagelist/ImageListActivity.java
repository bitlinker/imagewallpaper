package com.github.bitlinker.imagewallpaper.ui.imagelist;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.github.bitlinker.imagewallpaper.R;
import com.github.bitlinker.imagewallpaper.data.utils.UiUtils;
import com.github.bitlinker.imagewallpaper.di.Injector;
import com.github.bitlinker.imagewallpaper.di.UiScope;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ImageListActivity extends MvpAppCompatActivity implements ImageListView {
    private static final int PICK_IMAGE = 1;
    private static final String MIME_TYPE_IMAGE = "image/*";

    @InjectPresenter
    ImageListPresenter presenter;

    private UiScope uiScope;

    @BindView(android.R.id.content)
    View content;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    // TODO: selection tracker to presenter

    private ListAdapter<ImageInfo, ImageViewHolder> adapter;
    private SelectionTracker<Long> selectionTracker;
    private boolean isShowDeleteBtn;

    @ProvidePresenter
    ImageListPresenter providePresenter() {
        return uiScope.provideImageListPresenter();
    }

    // TODO: up?
    class ImageViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        @BindView(R.id.imgImage)
        ImageView imageView;

        private @Nullable
        ImageInfo imageInfo;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }

        public void setItem(@NonNull ImageInfo item, boolean isActivated) {
            this.imageInfo = item;
            itemView.setActivated(isActivated);
            Glide
                    .with(ImageListActivity.this)
                    .load(item.getUrl())
                    .thumbnail(0.5F)
                    .into(imageView);
            itemView.setOnLongClickListener(v -> {
                presenter.onImageLongClicked();
                return true;
            });
            itemView.setOnClickListener(view -> presenter.onImageClicked());
        }

        public ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
            return new ItemDetailsLookup.ItemDetails<Long>() {
                @Override
                public int getPosition() {
                    return getAdapterPosition();
                }

                @Nullable
                @Override
                public Long getSelectionKey() {
                    if (imageInfo == null) return null;
                    return getItemId();
                }
            };
        }
    }

    private static class ImageItemDetailsLookup extends ItemDetailsLookup<Long> {
        private final RecyclerView recyclerView;

        ImageItemDetailsLookup(RecyclerView recyclerView) {
            this.recyclerView = recyclerView;
        }

        @Nullable
        @Override
        public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                return ((ImageViewHolder) recyclerView.getChildViewHolder(view)).getItemDetails();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        uiScope = Injector.getInstance().createUiScope(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_list);
        ButterKnife.bind(this);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        fab.setOnClickListener(view -> presenter.onAddImageClicked());

        int spanCount = UiUtils.calculateNumOfColumns(this, getResources().getDimension(R.dimen.activity_image_list_column_size));
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));

        adapter = new ListAdapter<ImageInfo, ImageViewHolder>(new ImageDiffer()) {
            @Override
            public @NonNull
            ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View inflated = LayoutInflater.from(ImageListActivity.this).inflate(R.layout.activity_image_list_item, parent, false);
                return new ImageViewHolder(inflated);
            }

            @Override
            public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
                boolean isSelected = selectionTracker.isSelected((long) position);
                holder.setItem(getItem(position), isSelected);
            }

            @Override
            public long getItemId(int position) {
                return (long) position;
            }
        };
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);

        selectionTracker = new SelectionTracker.Builder<>(
                "imageSelection",
                recyclerView,
                new StableIdKeyProvider(recyclerView),
                new ImageItemDetailsLookup(recyclerView),
                StorageStrategy.createLongStorage()
        ).withSelectionPredicate(
                SelectionPredicates.createSelectAnything()
        ).build();
    }

    @Override
    public void showDeleteBtn(boolean value) {
        isShowDeleteBtn = value;
        supportInvalidateOptionsMenu();
    }

    @Override
    public void showAddBtn(boolean value) {
        if (value)
            fab.hide();
        else
            fab.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO: hide if not selection mode?
        MenuItem item = menu.add("Delete"); // TODO: id
        item.setIcon(R.drawable.ic_delete_black_24dp);
        item.setEnabled(isShowDeleteBtn);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        // TODO: handle delete
        presenter.onDeleteClicked(selectionTracker.getSelection());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void submitList(@NonNull List<ImageInfo> list) {
        adapter.submitList(list);
    }

    @Override
    public void startImagePicker() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(MIME_TYPE_IMAGE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }
        Intent pickIntent = new Intent(Intent.ACTION_PICK);
        pickIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MIME_TYPE_IMAGE);
        Intent chooserIntent = Intent.createChooser(intent, getString(R.string.activity_image_list_title_select_image));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    @Override
    public void setSelectionMode(boolean isSelectionMode) {
        // TODO: allow selection
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE) {
            List<String> uris = new ArrayList<>();
            if (data != null) {
                if (data.getData() != null) {
                    uris.add(data.getData().toString());
                } else {
                    ClipData clipData = data.getClipData();
                    if (clipData != null) {
                        for (int i = 0; i < clipData.getItemCount(); ++i) {
                            Uri uri = clipData.getItemAt(i).getUri();
                            uris.add(uri.toString());
                        }
                    }
                }
            }
            presenter.onImagePickerResult(uris);
        }
    }

    @Override
    public void showError(ErrorType type) {
        int id = R.string.error_generic;
        switch (type) {
            case AAD_IMAGE_ERROR:
                id = R.string.error_add_image;
                break;
        }
        Snackbar.make(content, id, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            try {
                uiScope.close();
            } catch (IOException e) {
                Timber.e(e);
            }
        }
        super.onDestroy();
    }
}
