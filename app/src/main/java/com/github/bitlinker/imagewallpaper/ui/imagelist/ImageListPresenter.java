package com.github.bitlinker.imagewallpaper.ui.imagelist;

import android.net.Uri;

import androidx.recyclerview.selection.Selection;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.github.bitlinker.imagewallpaper.business.ImageListInteractor;
import com.github.bitlinker.imagewallpaper.data.schedulers.SchedulerProvider;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

@InjectViewState
public class ImageListPresenter extends MvpPresenter<ImageListView> {
    private final ImageListInteractor interactor;
    private final SchedulerProvider schedulerProvider;

    private CompositeDisposable disposable = new CompositeDisposable();
    private boolean isSelectionMode;

    public ImageListPresenter(ImageListInteractor interactor,
                              SchedulerProvider schedulerProvider) {
        this.interactor = interactor;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        getViewState().setSelectionMode(false);
        updateList();
    }

    private void updateList() {
        disposable.add(
                interactor.getImageList()
                        .observeOn(schedulerProvider.ui())
                        .subscribe(
                                imageInfos -> getViewState().submitList(imageInfos),
                                this::showError
                        )
        );
    }

    public void onImageClicked() {
        if (isSelectionMode) return;

        // Select in selection?
        // TODO: navigate to preview
        // TODO: dialogfragment with setCancellable(false) makes it modal
    }

    public void onImageLongClicked() {
        if (isSelectionMode) return;

        isSelectionMode = true;
        getViewState().setSelectionMode(isSelectionMode);
    }

    public void onAddImageClicked() {
        getViewState().startImagePicker();
    }

    public void onImagePickerResult(List<String> data) {
        disposable.add(
                interactor.addImagesByUris(data)
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        this::updateList,
                        this::showError
                )
        );
    }

    public void onDeleteClicked(Selection<Long> selection) {
        // TODO: del
//        for (long i : selection) {
//            images.removeAll(i);
//        }
    }

    private void showError(Throwable throwable) {
        // TODO: remap
        getViewState().showError(ImageListView.ErrorType.GENERIC_ERROR);
    }

    @Override
    public void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }
}
