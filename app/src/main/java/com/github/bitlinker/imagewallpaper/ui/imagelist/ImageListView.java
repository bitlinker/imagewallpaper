package com.github.bitlinker.imagewallpaper.ui.imagelist;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.github.bitlinker.imagewallpaper.domain.ImageInfo;

import java.util.List;

public interface ImageListView extends MvpView {

    enum ErrorType {
        AAD_IMAGE_ERROR,
        GENERIC_ERROR
    }

    @StateStrategyType(AddToEndSingleStrategy.class)
    void submitList(@NonNull List<ImageInfo> list);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void startImagePicker();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setSelectionMode(boolean isSelectionMode);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showDeleteBtn(boolean value);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showAddBtn(boolean value);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showError(ErrorType type);
}
