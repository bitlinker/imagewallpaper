package com.github.bitlinker.imagewallpaper.ui.launcher;

import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;

import com.github.bitlinker.imagewallpaper.ui.MandatoryPermissionActivity;
import com.github.bitlinker.imagewallpaper.data.wallpaper.ImageWallpaperService;

/**
 * Launcher activity used to show live wallpaper selection system UI.
 * @author bitlinker
 */
public class StartPreviewActivity extends MandatoryPermissionActivity {
    @Override
    public void onCreateWithPermissions() {
        super.onCreateWithPermissions();
        Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
        intent.putExtra(
                WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
                new ComponentName(StartPreviewActivity.this, ImageWallpaperService.class)
        );
        startActivity(intent);
        finish();
    }
}
