package com.github.bitlinker.imagewallpaper.ui.settings;

import android.content.SharedPreferences;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

/**
 * The {@link PreferenceFragmentCompat} which listens to every preference change while visible
 */
public abstract class PreferenceObserverFragmentCompat extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        onPreferenceChanged(findPreference(key), key);
    }

    protected abstract void onPreferenceChanged(Preference preference, String key);
}
