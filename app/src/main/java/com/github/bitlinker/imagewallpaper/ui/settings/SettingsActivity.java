package com.github.bitlinker.imagewallpaper.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreferenceCompat;

import com.github.bitlinker.imagewallpaper.R;
import com.github.bitlinker.imagewallpaper.ui.MandatoryPermissionActivity;
import com.github.bitlinker.imagewallpaper.ui.imagelist.ImageListActivity;

public class SettingsActivity extends MandatoryPermissionActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MyPreferenceFragment())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // TODO: custom prefernce fragment compat to allow setting click/summary update

    public static class MyPreferenceFragment extends PreferenceObserverFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.settings, rootKey);
            PreferenceManager.setDefaultValues(getContext(), R.xml.settings, false);

            // TODO
            findPreference(getString(R.string.pref_images_list)).setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(getActivity(), ImageListActivity.class);
                startActivity(intent);
                return true;
            });

            initSummary(getPreferenceScreen());
        }

        private void initSummary(Preference p) {
            if (p instanceof PreferenceGroup) {
                PreferenceGroup pGrp = (PreferenceGroup) p;
                for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                    initSummary(pGrp.getPreference(i));
                }
            } else {
                updateSummary(p);
            }
        }

        private void updateSummary(Preference p) {
            if (p instanceof SwitchPreferenceCompat) {
                p.setSummary(((SwitchPreferenceCompat) p).isChecked() ?
                        android.R.string.yes :
                        android.R.string.no);
            } else if (p instanceof ListPreference) {
                p.setSummary(((ListPreference) p).getEntry());
            } else if (p instanceof EditTextPreference) {
                p.setSummary(((EditTextPreference) p).getText());
            }
        }


        @Override
        protected void onPreferenceChanged(Preference preference, String key) {
            updateSummary(preference);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // TODO: invalidate options
    }
}
